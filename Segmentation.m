function Segmentation()

% SEGMENTATION FOR GCAMP


%% Source Files (will convert ALL the .avi in the folder)
srcPath = uigetdir('Select the sequence path'); %Images Location
mkdir(srcPath, [filesep 'Segmented']);
srcFiles = strcat(srcPath,[filesep '*.tif']);  % the folder in which ur images exists
srcFiles = dir(srcFiles);
[x,y] = size(srcFiles);


prompt = {'filter size', 'background', 'minimum object size', 'maximum object size'};
title = 'Parameters';
definput = {'30', '3', '100','inf'};
answer = inputdlg(prompt,title,[1 50],definput);
filter_size= str2double(answer{1});
background= str2double(answer{2});
minsize= str2double(answer{3});
maxsize= str2double(answer{4});



%% The segmentation

    for Files=1:x
        
        disp(strcat('analysing',{' '}, srcFiles(Files).name))
   
        I = read_stackTiff(strcat(srcPath,'/',srcFiles(Files).name));
        
        FileName=(char(srcFiles(Files).name));
        FileName=FileName (:,1:end-4);
        
       
 tic   
        se = strel('disk',filter_size);   
        tophatFiltered = imtophat(I,se);   
        mip2 = max(tophatFiltered, [], 3);
        % mip = max(I, [], 3);   

        meanmip=imfilter(mip2,fspecial('average',filter_size),'replicate');
        sImat=mip2-meanmip-background;
        bwImat=im2bw(sImat,0);
    
toc

        BW=bwareafilt (bwImat, [minsize maxsize],4);
        BW=imfill(BW,'holes');

    
        disp('saving images')
                
                outputFileName = strcat(srcPath,'\Segmented\', FileName, '_mask.tif');
                imwrite(uint16((BW)),outputFileName, 'WriteMode', 'append',  'Compression','none');
                
            
            
        
            
    end
                       
         disp('Done - enjoy! :)')
    
    
    



